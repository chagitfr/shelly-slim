<?php
require "bootstrap.php";
use Chatter\Middleware\Logging;
//Q2
use Chatter\Models\User;

$app = new \Slim\App();
$app->add(new Logging());

//Q2
//delete can be with get
$app->get('/users', function($request, $response, $args){
    $token = $request->getQueryParam('token','');
if($token=='12345')
{ 

    $_user = new User();
    $users = $_user->all();
    $payload = [];
        foreach($users as $user){
            $payload[$user->id] = [
                "name" => $user->name,
                "phonenumber" => $user->phonenumber
            ];
    }
    
    return $response->withStatus(200)->withJson($payload);
}
 else{
 return $response->withStatus(405)->withJson($payload);
 }

    
});


//Q3
$app->post('/users', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
    $token = $request->getParsedBodyParam('token','');
    if($token=='12345')
    { 
        $_user = new User();
        $_user->name = $name;
        $_user->phonenumber = $phonenumber;
        $_user->save();
        if($_user->id){
            $payload = ['user_id' => $_user->id];
            return $response->withStatus(201)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
    }
    else{
        return $response->withStatus(405)->withJson($payload);
    }
});

//Q4 1
$app->delete('/users/{user_id}', function($request, $response,$args){
    $token = $args['token'];
    if($token=='12345')
    { 
        $user = User::find($args['user_id']);
        $user->delete();
        
        if($user->exists){
            return $response->withStatus(400);
        }
        else{
        return $response->withStatus(200)->withJson($payload);
    }
    }
    else{
        return $response->withStatus(404)->withJson($payload);
    }  
});

//Q4 2
$app->put('/users/{user_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
        $_user = User::find($args['user_id']);
        $_user->name = $name;
        $_user->phonenumber = $phonenumber;

        if($_user->save()){ //אם הערך נשמר
            $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }  
    else{
            return $response->withStatus(400);
        } 
});

$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
        $message = User::find($_id);
        return $response->withStatus(200)->withJson($message);
});

//Login without JWT
$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});


//"path" => ['/messages','/users']
//Login with token
$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');
    
    if($user=='jack' && $password=='1234'){
        $payload = ['token'=>'12345'];
       // $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/users');
        return $response->withStatus(200)->withJson($payload); 
    }
    else{
        $payload = ['token'=>null];
        return $response->withStatus(403)->withJson($payload);
    }
});

//אבטחת מידע
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();